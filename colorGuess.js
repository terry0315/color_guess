// let colors = [
//   'rgb(255, 0, 0)',
//   'rgb(255, 255, 0)',
//   'rgb(0, 255, 0)',
//   'rgb(0, 255, 255)',
//   'rgb(0, 0, 255)',
//   'rgb(255, 0, 255)',
// ];

// array of different RGB colors
let numberOfSquares = 6;
let colors = generateRandomColors(numberOfSquares);
let squares = document.querySelectorAll('.square');
let colorDisplay = document.querySelector('#colorDisplay');
let messageDisplay = document.querySelector('#message');
let h1 = document.querySelector('h1');
let resetGameButton = document.querySelector('button');
let pickedColor = pickColor();
let easyBtn = document.querySelector('#easyBtn');
let hardBtn = document.querySelector('#hardBtn');
let message = "Hello Max!!!!";

// add click event for easy button
easyBtn.addEventListener('click', function () {
	console.log('easy button');
	easyBtn.classList.add('selected');
	hardBtn.classList.remove('selected');
	// generate new colors
	numberOfSquares = 3;
	colors = generateRandomColors(numberOfSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;

	for (let i = 0; i < squares.length; i++) {
		// check if there is color in that index
		if (colors[i]) {
			squares[i].style.backgroundColor = colors[i];
		} else {
			squares[i].style.display = 'none';
		}
	}
});

// add click event for hard button
hardBtn.addEventListener('click', function () {
	console.log('hard button');
	hardBtn.classList.add('selected');
	easyBtn.classList.remove('selected');

	// generate new colors
	numberOfSquares = 6;
	colors = generateRandomColors(numberOfSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;

	for (let i = 0; i < squares.length; i++) {
		// check if there is color in that index
		squares[i].style.backgroundColor = colors[i];
		squares[i].style.display = 'block';
	}
});

// display color in RGB
colorDisplay.textContent = pickedColor;

// loop through all the squares
for (let i = 0; i < squares.length; i++) {
	// add colors to all the squares
	squares[i].style.backgroundColor = colors[i];

	// add click events for each of the square
	squares[i].addEventListener('click', function () {
		// console.log(squares[i].style.backgroundColor);

		// correct
		if (squares[i].style.backgroundColor === pickedColor) {
			// console.log('you guessed it!!!');
			messageDisplay.textContent = 'Correct!';
			h1.style.backgroundColor = pickedColor;
			changeColors(pickedColor);
			resetGameButton.textContent = 'Play Again?';
		} else {
			// incorrect
			// console.log('wrong pick!!!');
			this.style.backgroundColor = '#232323';
			messageDisplay.textContent = 'Try Again';
		}
	});
}

// change all colors of the square
function changeColors(color) {
	for (let i = 0; i < squares.length; i++) {
		squares[i].style.backgroundColor = color;
	}
}

// select the color to be guessed
function pickColor() {
	let test = 123;
	let random = Math.floor(Math.random() * colors.length);
	return colors[random];
}

// push generated colors into array
function generateRandomColors(num) {
	let arr = [];

	for (let i = 0; i < num; i++) {
		// console.log(randomColor());
		arr.push(randomColor());
	}
	console.log(arr);
	return arr;
}

// generate random rgb colors
function randomColor() {
	let red = Math.floor(Math.random() * 255 + 1);
	let green = Math.floor(Math.random() * 255 + 1);
	let blue = Math.floor(Math.random() * 255 + 1);

	return `rgb(${red}, ${green}, ${blue})`;
}

// reset the game
resetGameButton.addEventListener('click', function () {
	console.log('reset');
	messageDisplay.textContent = '';
	// randomize colors
	// numberOfSquares = 6;
	colors = generateRandomColors(numberOfSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	h1.style.backgroundColor = 'steelblue';

	for (let i = 0; i < squares.length; i++) {
		squares[i].style.backgroundColor = colors[i];
	}
	this.textContent = 'New Colors';
});
